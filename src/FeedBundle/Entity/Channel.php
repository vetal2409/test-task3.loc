<?php

namespace FeedBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * Channel
 *
 * @ORM\Table(name="channel")
 * @ORM\Entity(repositoryClass="FeedBundle\Repository\ChannelRepository")
 */
class Channel
{
    const TYPE_RSS = 'rss';
    const TYPE_FACEBOOK = 'facebook';
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="type", length=10)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="address")
     */
    private $address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="imported_at", nullable=true)
     */
    private $importedAt;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User", mappedBy="channels")
     */
    private $users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->registrations = new ArrayCollection();
    }
    

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Channel
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set importedAt
     *
     * @param \DateTime $importedAt
     *
     * @return Channel
     */
    public function setImportedAt($importedAt)
    {
        $this->importedAt = $importedAt;

        return $this;
    }

    /**
     * Get importedAt
     *
     * @return \DateTime
     */
    public function getImportedAt()
    {
        return $this->importedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add user
     *
     * @param User $user
     *
     * @return Channel
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Channel
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}

<?php

namespace FeedBundle\Controller;

use FeedBundle\Entity\Channel;
use FeedBundle\Entity\Feed;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class FeedController extends Controller
{
    /**
     * @Route("/feeds", name="feed_index")
     * @param Channel $channel
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('@Feed/feed/index.html.twig', [
            'channels' => $this->getChannels(),
            'feeds' => []
        ]);
    }

    /**
     * @Route("/feed/channel/{id}", name="feed_get")
     * @param Channel $channel
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAction(Channel $channel)
    {
        /**
         * @var $feeds Feed[]
         */
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('FeedBundle:Feed');
        $feeds = $repository->findBy(['channel' => $channel]);
        return $this->render('@Feed/feed/index.html.twig', [
            'channels' => $this->getChannels(),
            'feeds' => $feeds
        ]);
    }

    public function getChannels()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('FeedBundle:Channel');
        return $repository->findAll();
    }
}

<?php

namespace FeedBundle\Controller;

use DateTime;
use Debril\RssAtomBundle\Protocol\Parser\FeedContent;
use Debril\RssAtomBundle\Protocol\Parser\Item;
use FeedBundle\Entity\Feed;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FeedBundle\Entity\Channel;
use FeedBundle\Form\ChannelType;
use UserBundle\Entity\User;

/**
 * Channel controller.
 *
 * @Route("/channel")
 */
class ChannelController extends Controller
{
    /**
     * Lists all Channel entities.
     *
     * @Route("/", name="channel_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $channels = $em->getRepository('FeedBundle:Channel')->findAll();

        return $this->render('channel/index.html.twig', array(
            'channels' => $channels,
        ));
    }

    /**
     * Creates a new Channel entity.
     *
     * @Route("/new", name="channel_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $channel = new Channel();
        $user = $this->getUser();
        $form = $this->createForm('FeedBundle\Form\ChannelType', $channel);
        $form->handleRequest($request);
        $channel->addUser($user);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($channel);
            $em->flush();

            return $this->redirectToRoute('channel_show', array('id' => $channel->getId()));
        }

        return $this->render('channel/new.html.twig', array(
            'channel' => $channel,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Channel entity.
     *
     * @Route("/{id}", name="channel_show")
     * @Method("GET")
     */
    public function showAction(Channel $channel)
    {
        $deleteForm = $this->createDeleteForm($channel);

        return $this->render('channel/show.html.twig', array(
            'channel' => $channel,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Channel entity.
     *
     * @Route("/{id}/edit", name="channel_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Channel $channel)
    {
        $deleteForm = $this->createDeleteForm($channel);
        $editForm = $this->createForm('FeedBundle\Form\ChannelType', $channel);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($channel);
            $em->flush();

            return $this->redirectToRoute('channel_edit', array('id' => $channel->getId()));
        }

        return $this->render('channel/edit.html.twig', array(
            'channel' => $channel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Channel entity.
     *
     * @Route("/{id}", name="channel_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Channel $channel)
    {
        $form = $this->createDeleteForm($channel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($channel);
            $em->flush();
        }

        return $this->redirectToRoute('channel_index');
    }

    /**
     * Creates a form to delete a Channel entity.
     *
     * @param Channel $channel The Channel entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Channel $channel)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('channel_delete', array('id' => $channel->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @Route("/{id}/import", name="channel_import_feeds")
     */
    public function importFeedsAction(Channel $channel)
    {
        /**
         * @var $feedContent FeedContent
         * @var $items Item[]
         */
        $em = $this->getDoctrine()->getManager();


        $reader = $this->get('debril.reader');

        $feedContent = $reader->getFeedContent($channel->getAddress(), $channel->getImportedAt());
        $items = $feedContent->getItems();

        $channel->setImportedAt($feedContent->getLastModified());
        $em->persist($channel);
        foreach ($items as $item) {
            $feed = new Feed();
            $feed
                ->setChannel($channel)
                ->setTitle($item->getTitle())
                ->setLink($item->getLink())
                ->setDescription($item->getDescription());
            $em->persist($feed);
        }
        $em->flush();
        var_dump($item);
        return new Response();
    }


}
